package com.steps;

import java.lang.reflect.Method;
import com.Reporter.ExtentTestManager;
import com.Reporter.TestListener;
import com.pageObjects.Checkout;
import com.pageObjects.HomePage;
import com.pageObjects.LoginPage;
import com.pageObjects.OrderDetails;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.*;

public class Tests {

    public static WebDriver driver;
    public LoginPage loginpage = new LoginPage();
    public HomePage homePage = new HomePage();
    public Checkout checkoutPage = new Checkout();
    public OrderDetails orderDetails = new OrderDetails();

    @Parameters({"url"})
    @BeforeClass
    public void setUp(String url){

        System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver");
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", System.getProperty("user.dir")+"/src/downloads");

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(url);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        waitForElement(loginpage.getSignInButton());

    }

    @Test(priority = 1, enabled = false, groups = {"sanity", "regression"})
    //@Parameters()
    public void createAccount(Method method){
        ExtentTestManager.startTest(method.getName(), "Create Account");
        String[] fields = {"customer_firstname","customer_lastname",
                 "passwd", "firstname",
                "lastname", "company", "address1",
                "address2", "city", "postcode",
                "phone_mobile", "phone", "alias",
                "id_gender1", "newsletter", "optin"};

        String[] checks = {"id_gender1", "newsletter", "optin"};



        //driver.get("http://automationpractice.com/index.php");
        driver.findElement(loginpage.getSignInButton());
        driver.findElement(loginpage.getSignInButton()).click();
        waitForElement(loginpage.getCreateAccountButton());
        driver.findElement(loginpage.getCreateEmailInput()).sendKeys("miguel.miguel@miguel.com");
        driver.findElement(loginpage.getCreateAccountButton()).click();
        waitForElement(loginpage.getRegisterButton());
        for (String id : fields) {

            if(id.equals("phone") || id.equals("phone_mobile") || id.equals("postcode")){
                driver.findElement(loginpage.getInputFieldsWhitId(id)).sendKeys("12345");
            }else{
                if(id.equals("id_gender1") || id.equals("newsletter") || id.equals("optin")){
                    driver.findElement(loginpage.getInputFieldsWhitId(id)).click();

                }else{
                    driver.findElement(loginpage.getInputFieldsWhitId(id)).sendKeys("TestTest");
                }
            }
        }
        setOptionSelect(loginpage.getDaysSelect());
        setOptionSelect(loginpage.getMonthsSelect());
        setOptionSelect(loginpage.getYearsSelect());
        setOptionSelect(loginpage.getPersonalCountrySelect());
        setOptionSelect(loginpage.getPersonalStateSelect());

        driver.findElement(loginpage.getRegisterButton()).click();
        waitForElement(homePage.getMyAccountTitle());
        Assert.assertTrue(driver.findElement(homePage.getMyAccountTitle()).isDisplayed(), "My Account title is not displayed");





    }
    @Test(priority =2, enabled = false, description = "Logout", groups = {"integration","sanity", "regression"})
    public void Logout(Method method){
        ExtentTestManager.startTest(method.getName(), "Logout");
        waitForElement(homePage.getSignOutButton());
        driver.findElement(homePage.getSignOutButton()).click();
        waitForElement(loginpage.getSignInButton());
        Assert.assertTrue(driver.findElement(loginpage.getSignInButton()).isDisplayed(), "Sign In Button is not displayed");

    }

    @Test(priority=3, enabled = true, groups = {"integration","sanity", "regression"})
    public void Login(Method method){
        ExtentTestManager.startTest(method.getName(), "Login");

        driver.findElement(loginpage.getSignInButton());
        driver.findElement(loginpage.getSignInButton()).click();
        waitForElement(loginpage.getCreateAccountButton());
        driver.findElement(loginpage.getEmailLogin()).sendKeys("miguel.miguel@miguel.com");
        driver.findElement(loginpage.getPasswdLogin()).sendKeys("TestTest");
        driver.findElement(loginpage.getLoginButton()).click();
        waitForElement(homePage.getMyAccountTitle());
        Assert.assertTrue(driver.findElement(homePage.getMyAccountTitle()).isDisplayed(), "My Account title is not displayed");




    }
    @Test(priority=4, enabled = false, groups = {"integration","sanity", "regression"})
    public void SearchForWord(Method method){
        ExtentTestManager.startTest(method.getName(), "Search Words");
        waitForElement(homePage.getTopSearchField());
        driver.findElement(homePage.getTopSearchField()).sendKeys("Dress");
        driver.findElement(homePage.getSearchButton()).click();
        waitForElement(homePage.getDressResults());
        int numberResults= driver.findElements(homePage.getDressResults()).size();
        System.out.println("The Number of Results is: " + numberResults);
        boolean correctResults = true;
        for(WebElement result: driver.findElements(homePage.getDressResults())){

            if(!result.getAttribute("title").toLowerCase().contains("dress")){
                correctResults = false;
            }

        }
        Assert.assertTrue(correctResults, "There are results not related with the search");


    }

    @Test(priority =5, enabled = false, groups = {"integration","sanity", "regression"})
    public void AddToCart(Method method) throws InterruptedException {
        ExtentTestManager.startTest(method.getName(), "Add to cart");
        if(driver.findElements(homePage.getDressResults()).size() == 0){
            waitForElement(homePage.getTopSearchField());
            driver.findElement(homePage.getTopSearchField()).sendKeys("Dress");
            driver.findElement(homePage.getSearchButton()).click();
            waitForElement(homePage.getDressResults());
            int numberResults= driver.findElements(homePage.getDressResults()).size();
            System.out.println("The Number of Results is: " + numberResults);
        }
        driver.findElements(homePage.getDressResults()).get(0).click();
        sleep(3000);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", driver.findElement(homePage.getAddToCarButton()));
        waitForElement(homePage.getConfirmationMessageAdditionToCar());
        Assert.assertTrue(driver.findElement(homePage.getConfirmationMessageAdditionToCar()).isDisplayed(), "Error on confirmation message after adding to the car");
        driver.findElement(homePage.getProceedToCheckOutButton()).click();
        waitForElement(checkoutPage.getSummaryTitle());
        //js.executeScript("window.scrollBy(0,1000)");
        int i = 0;
        while(i<3){
            waitForElement(checkoutPage.getProceedToCheckOutButtonSteps());
            if(i == 2){
                driver.findElement(checkoutPage.getTermsAndConditions()).click();
                driver.findElement(checkoutPage.getProceedToCheckOutButtonSteps()).click();

            }else{
                driver.findElement(checkoutPage.getProceedToCheckOutButtonSteps()).click();
            }
            i++;
        }
        waitForElement(checkoutPage.getPaymentButton());
        driver.findElement(checkoutPage.getPaymentButton()).click();
        waitForElement(checkoutPage.getConfirmOrderButton());
        driver.findElement(checkoutPage.getConfirmOrderButton()).click();

        waitForElement(checkoutPage.getConfirmationOrderStrong());
        Assert.assertTrue(driver.findElement(checkoutPage.getConfirmationOrderStrong()).isDisplayed());



    }
    @Test(enabled = false, priority = 6, groups = {"integration","sanity", "regression"})
    public void DownloadPdf(Method method) throws InterruptedException {

        ExtentTestManager.startTest(method.getName(), "Download PDF");
        waitForElement(homePage.getMyAccountButton());
        driver.findElement(homePage.getMyAccountButton()).click();
        waitForElement(homePage.getOrderDetailsButton());
        driver.findElement(homePage.getOrderDetailsButton()).click();
        waitForElement(orderDetails.getOrderHistory());
        String pdfLink = driver.findElements(orderDetails.getOrderDetails()).get(0).getAttribute("href");

        /*Dynamic way to get the name of the pdf file, but it is not possible to know what is the name of the file without check in the DB.
          A good approach could be to include the name of the Invoice in any tag to make easy automation when DB is not accessible
          */
        String pdfName ="IN"+pdfLink.substring(24, 30);
        System.out.println("This is the pdfName: " + pdfName +"/");
        //"javascript:showOrder(1, 162447, 'http://automationpractice.com/index.php?controller=order-detail');"

        driver.findElements(orderDetails.getOrderDetails()).get(0).click();
        waitForElement(orderDetails.getDownloadPDF());
        driver.findElement(orderDetails.getDownloadPDF()).click();

        Thread.sleep(5000);
        try {
            File pdf = new File(System.getProperty("user.dir")+"/src/downloads/"+pdfName+".pdf");
            if(!pdf.exists()){
                Assert.assertTrue(false, "PDF doesn't exist");
            }
        }catch (Exception e){
            System.out.println("There is not pdf");
        }


    }
    @Test(enabled = false, groups = {"integration","sanity", "regression"})
    public void FailedLogin(Method method){
        ExtentTestManager.startTest(method.getName(), "Failed Login");
        driver.findElement(loginpage.getSignInButton());
        driver.findElement(loginpage.getSignInButton()).click();
        waitForElement(loginpage.getCreateAccountButton());
        driver.findElement(loginpage.getEmailLogin()).sendKeys("miguel.migl@miguel.com");
        driver.findElement(loginpage.getPasswdLogin()).sendKeys("TestTe");
        driver.findElement(loginpage.getLoginButton()).click();
        waitForElement(homePage.getMyAccountTitle());
        Assert.assertTrue(driver.findElement(homePage.getMyAccountTitle()).isDisplayed(), "My Account title is not displayed");

    }

    @AfterClass
    public void close(){
        //driver.close();
        //driver.quit();
    }

    public void waitForElement(By element){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public void setOptionSelect(By selectName){
        Select selection = new Select (driver.findElement(selectName));
        selection.selectByIndex(1);
    }

}
