package com.pageObjects;

import org.openqa.selenium.By;

public class OrderDetails {
    By orderDetails = By.xpath("//a[@class='color-myaccount']");
    By orderHistoryTitle = By.xpath("//h1[text()='Order history']");
    By downloadPDF = By.xpath("//a[contains(text(), 'Download')]");

    public By getOrderDetails() {
        return orderDetails;
    }

    public By getOrderHistory() {
        return orderHistoryTitle;
    }

    public By getDownloadPDF() {
        return downloadPDF;
    }
}
