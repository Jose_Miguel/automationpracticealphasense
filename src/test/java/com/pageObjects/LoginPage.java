package com.pageObjects;

import org.openqa.selenium.By;

public class LoginPage {
    private By signInButton = By.xpath("//a[contains(text(),'Sign in')]");
    private By createAccountButton = By.id("SubmitCreate");
    private By createEmailInput = By.id("email_create");
    private By mrGenderRadio = By.id("id_gender1");
    private By mrsGenderRadio = By.id("id_gender2");
    private By firstNameInput = By.id("customer_firstname");
    private By lastNameInput = By.id("customer_lastname");
    private By emailInput = By.id("email");
    private By passwordInput = By.id("passwd");
    private By daysSelect = By.id("days");
    private By monthsSelect = By.id("months");
    private By yearsSelect = By.id("years");
    private By newsletterInput = By.id("newsletter");
    private By optinInput = By.id("optin");
    private By personalFirstNameInput = By.id("firstname");
    private By personalLastNameInput = By.id("lastname");
    private By companyInput = By.id("company");
    private By personalAddressInput = By.id("address1");
    private By personalCityInput = By.id("city");
    private By personalStateSelect = By.id("id_state");
    private By personalZipCodeInput = By.id("postcode");
    private By personalCountrySelect = By.id("id_country");
    private By personalMobileInput = By.id("phone_mobile");
    private By personalAddressAliasInput = By.id("alias");
    private By registerButton = By.id("submitAccount");

    private By emailLogin = By.id("email");

    public By getEmailLogin() {
        return emailLogin;
    }

    public By getPasswdLogin() {
        return passwdLogin;
    }

    public By getLoginButton() {
        return loginButton;
    }

    private By passwdLogin = By.id("passwd");
    private By loginButton = By.id("SubmitLogin");


    public By getSignInButton() {
        return signInButton;
    }

    public By getCreateAccountButton() {
        return createAccountButton;
    }

    public By getCreateEmailInput() {
        return createEmailInput;
    }

    public By getMrGenderRadio() {
        return mrGenderRadio;
    }

    public By getMrsGenderRadio() {
        return mrsGenderRadio;
    }

    public By getFirstNameInput() {
        return firstNameInput;
    }

    public By getLastNameInput() {
        return lastNameInput;
    }

    public By getEmailInput() {
        return emailInput;
    }

    public By getPasswordInput() {
        return passwordInput;
    }

    public By getDaysSelect() {
        return daysSelect;
    }

    public By getMonthsSelect() {
        return monthsSelect;
    }

    public By getYearsSelect() {
        return yearsSelect;
    }

    public By getNewsletterInput() {
        return newsletterInput;
    }

    public By getOptinInput() {
        return optinInput;
    }

    public By getPersonalFirstNameInput() {
        return personalFirstNameInput;
    }

    public By getPersonalLastNameInput() {
        return personalLastNameInput;
    }

    public By getCompanyInput() {
        return companyInput;
    }

    public By getPersonalAddressInput() {
        return personalAddressInput;
    }

    public By getPersonalCityInput() {
        return personalCityInput;
    }

    public By getPersonalStateSelect() {
        return personalStateSelect;
    }

    public By getPersonalZipCodeInput() {
        return personalZipCodeInput;
    }

    public By getPersonalCountrySelect() {
        return personalCountrySelect;
    }

    public By getPersonalMobileInput() {
        return personalMobileInput;
    }

    public By getPersonalAddressAliasInput() {
        return personalAddressAliasInput;
    }

    public By getRegisterButton() {
        return registerButton;
    }
    public By getInputFieldsWhitId(String id){
        return By.xpath("//input[@id=\"" + id + "\"]");
    }
}
