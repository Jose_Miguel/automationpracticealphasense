package com.pageObjects;

import org.openqa.selenium.By;

public class Checkout {
    By summaryTitle = By.xpath("//span[contains(text(), 'Summary')]");
    By signInTitle = By.xpath("//span[contains(text(), 'Sign in')]");
    By addressTitle = By.xpath("//span[contains(text(), 'Address')]");
    By shippingTitle = By.xpath("//span[contains(text(), 'Shipping')]");
    By paymentTitle = By.xpath("//span[contains(text(), 'Payment')]");
    By termsAndConditions = By.id("cgv");
    By proceedToCheckOutButtonSteps = By.xpath("(//span[contains(text(), 'Proceed to')])[2]");
    By paymentButton = By.xpath("//a[contains(@title,'Pay by bank wire')]");
    By confirmOrderButton = By.xpath("//span[contains(text(),'I confirm my order')]");
    By confirmationOrderStrong = By.xpath("//strong[contains(text(),'Your order on My Store is complete.')]");

    public By getConfirmationOrderStrong() {
        return confirmationOrderStrong;
    }

    public By getConfirmOrderButton() {
        return confirmOrderButton;
    }

    public By getPaymentButton() {
        return paymentButton;
    }

    public By getProceedToCheckOutButtonSteps() {
        return proceedToCheckOutButtonSteps;
    }

    public By getSummaryTitle() {
        return summaryTitle;
    }

    public By getSignInTitle() {
        return signInTitle;
    }

    public By getAddressTitle() {
        return addressTitle;
    }

    public By getShippingTitle() {
        return shippingTitle;
    }

    public By getPaymentTitle() {
        return paymentTitle;
    }

    public By getTermsAndConditions() {
        return termsAndConditions;
    }
}
