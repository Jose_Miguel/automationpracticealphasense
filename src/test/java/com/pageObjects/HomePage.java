package com.pageObjects;

import org.openqa.selenium.By;

public class HomePage {
    By myAccountTitle = By.xpath("//h1[text()='My account']");
    By TopSearchField = By.id("search_query_top");
    By DressResults = By.xpath("//img[@itemprop='image']");
    By signOutButton = By.xpath("//a[@title='Log me out']");
    By searchButton = By.name("submit_search");
    By addToCarButton = By.xpath("//span[text()='Add to cart']/..");
    By confirmationMessageAdditionToCar = By.className("icon-ok");
    By proceedToCheckOutButton = By.xpath("//span[contains(text(), 'Proceed to')]");

    By MyAccountButton = By.xpath("//a[@title='View my customer account']");
    By OrderDetailsButton = By.xpath("//a[@title='Orders']");


    public By getAddToCarButton() {
        return addToCarButton;
    }

    public By getOrderDetailsButton() {
        return OrderDetailsButton;
    }

    public By getConfirmationMessageAdditionToCar() {
        return confirmationMessageAdditionToCar;
    }

    public By getProceedToCheckOutButton() {
        return proceedToCheckOutButton;
    }

    public By getMyAccountButton() {
        return MyAccountButton;
    }

    public By getSignOutButton() {
        return signOutButton;
    }

    public By getSearchButton() {
        return searchButton;
    }

    public By getMyAccountTitle() {
        return myAccountTitle;
    }

    public By getTopSearchField() {
        return TopSearchField;
    }

    public By getDressResults() {
        return DressResults;
    }
}
